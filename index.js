const express = require('express');
const app = express();
const routerUser = require('./app/routes/routeUser');
const routerAuth = require('./app/routes/routeAuth');
const routerCalculator = require('./app/routes/routeCalculator');
const routerUploadFile = require('./app/routes/routeUploadFile');
const ErrorHandler = require('./app/controller/ErrorHandler');

const port = 8080;


app.use('/api', [routerUser,routerAuth,routerCalculator,routerUploadFile]);

app.use(ErrorHandler.error);

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})