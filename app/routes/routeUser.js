const express = require('express');
const router = express.Router();
const UserController = require('../controller/UserController');
const middleware = require('../middleware/validation');

router.use(express.json())

router.get("/user", UserController.getList);
router.post("/user/post", [middleware.valUserPost], UserController.postUser);
router.put("/user/put", [middleware.valUser], UserController.putUser);
router.delete("/user/delete", [middleware.valUser], UserController.deleteUser);

router.post("/checkage", [middleware.checkage], UserController.checkage);


module.exports = router;
