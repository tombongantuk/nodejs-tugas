const express = require('express');
const router = express.Router();
const AuthController = require('../controller/AuthController');
const middleware = require('../middleware/validation');

router.use(express.json())

router.post("/login", [middleware.valAuth], AuthController.userAuth);

module.exports = router;