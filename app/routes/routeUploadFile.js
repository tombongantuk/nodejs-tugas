const express = require('express');
const router = express.Router();
const UploadFileController = require('../controller/UploadFileController');
const upload = require('../middleware/uploadFile');

router.use(express.json());

router.post('/upload', upload.single('uploaded_file'), UploadFileController.uploadFile);

module.exports = router;
