const express = require('express')
const router = express.Router();
const Calculator = require('./../controller/CalculatorController');
const middleware = require('../middleware/validation');

router.use(express.json());

router.post('/calculator/bagi', [middleware.valDenumeratorAndNumerator], Calculator.bagi);
router.post('/calculator/tambah', [middleware.valArrayOfNumber], Calculator.tambah);
router.post('/calculator/kurang', [middleware.valArrayOfNumber], Calculator.kurang);
router.post('/calculator/kali', [middleware.valArrayOfNumber], Calculator.kali);

module.exports = router;