const { body, validationResult } = require('express-validator');

/**
 * [validate description]
 * memvalidasi input body dari request express-validator
 * @var {[type]}
 */
const validate = validations => {
    return async (req, res, next) => {
        await Promise.all(validations.map(validation => validation.run(req)));

        const errors = validationResult(req);
        if (errors.isEmpty()) {
            return next();
        }

        res.status(400).json({ errors: errors.array() });
    };
};

/**
 * [valArrayOfNumber description]
 *  cek input request body berdasarkan condisi tertentu
 * @return  {[]}      [return description]
 */
exports.valArrayOfNumber = validate([
    body("nilai").exists({ checkFalsy: true, checkNull: true })
        .withMessage("masukan field nilai").bail(),
    body("nilai").isArray()
        .withMessage("nilai harus berupa array").bail(),
    body("nilai").isArray({ min: 2 })
        .withMessage("isi array nilai minimal 2").bail(),
    body("nilai.*").isNumeric()
        .withMessage("array nilai harus berupa angka").toFloat()
]);

/**
 * [valDenumeratorAndNumerator description]
 *cek input request body berdasarkan condisi tertentu
 * @return  {[]}      [return description]
 */
exports.valDenumeratorAndNumerator = validate([
    body("nilai1").exists({ checkFalsy: false, checkNull: true })
        .withMessage("masukan field nilai-1").bail(),
    body("nilai2").exists({ checkFalsy: false, checkNull: true })
        .withMessage("masukan field nilai-2").bail(),
    body("nilai1").isNumeric()
        .withMessage("nilai-1 harus berupa angka").toFloat(),
    body("nilai2").isNumeric()
        .withMessage("nilai-2 harus berupa angka").toFloat()
]);

/**
 * [valAuth description]
 * memvalidasi input body untuk Login
 * @return  {[]}      [return description]
 */
exports.valAuth = validate([
    body("username").exists({ checkFalsy: true, checkNull: true })
        .withMessage("masukan field username").bail(),
    body("password").exists({ checkFalsy: true, checkNull: true }).trim()
        .withMessage("masukan field password").bail(),
    body("username").isString().trim()
        .withMessage("username berupa string").bail(),
]);


/**
 * [valUserPost description]
 * memvalidasi input body untuk memasukan user
 * @return  {[]}      [return description]
 */
exports.valUserPost = validate([
    body("name").exists({ checkFalsy: true, checkNull: true })
        .withMessage("masukan field name").bail(),
    body("email").exists({ checkFalsy: true, checkNull: true })
        .withMessage("masukan field email").bail(),
    body("name").trim().isString({min:5})
        .withMessage("nama berupa string dan minimal 5 karakter").bail(),
    body("email").trim().isEmail().normalizeEmail()
        .withMessage("field email harus sesuai format email").bail(),
]);

/**
 * [valUser description]
 *memvalidasi input body untuk put,delete user
 * @return  {[]}      [return description]
 */
exports.valUser = validate([
    body("id").exists({ checkFalsy: true, checkNull: true })
        .withMessage("masukan field id").bail(),
    body("id").trim().isInt()
        .withMessage("field harus berupa integer").bail(),
]);

exports.checkage = validate([
    body("dob").exists({ checkFalsy: false, checkNull: true })
        .withMessage("masukan field dob").bail(),
    body("dob").isDate()
        .withMessage('field dob harus tanggal').bail(),
    body("dob").isISO8601()
        .withMessage('field dob sesuai format yyyy-mm-dd').bail(),
]);