const { json } = require('express');
const multer = require('multer');
const path = require('path');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './app/public/my-uploads')
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + path.extname(file.originalname);
        cb(null, file.fieldname + '-' + uniqueSuffix)
    },
})
let upload = multer({
    storage: storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype == 'image/jpg' || file.mimetype == 'image/jpeg' || file.mimetype == 'image/png') {
            cb(null, true);
        }
        else {
            return cb(new Error('file harus berupa gambar(jpg,jpeg,png)'));
        }
    },
})

module.exports = upload;