const helper = require('./../Helper/format');

exports.checkage =  (req, res) => {
    const dateNow = new Date();
    const db = new Date(req.body.dob);
    const age = dateNow.getFullYear() - db.getFullYear();
    const month = dateNow.getMonth() - db.getMonth();
    
    const dob = {
        DoB:req.body.dob,
    }
    if (month < 0 || (month === 0 && dateNow.getDate() < db.getDate())) {
        age = age - 1;
    }
    if (age < 17) {
        return res.json({error:"your are not 17 years old or older yet"})
    }
    else {
        return res.json(helper.formats("success",dob));
    }

}

exports.getList = (req, res) => {
    return res.json("List user");
}

exports.postUser = (req, res) => {
    const user = {
        name: req.body.name,
        email: req.body.email
    }
    return res.json(helper.formats("success", user));
}

exports.putUser = (req, res) => {
    const user = {
        id: req.body.id,
    }
    return res.json(helper.formats("updated", user));
}

exports.deleteUser = (req,res) => {
    const user = {
        id: req.body.id,
    }
    return res.json(helper.formats("deleted", user));
}