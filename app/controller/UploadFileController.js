const path = require('path');
const multer = require('multer');
const helper = require('../Helper/format');
const { json } = require('express');

exports.uploadFile = (req, res,next) => {
    
    if (req.file == undefined) {
        return res.json({ message: "Please upload a file!" });
    
    }else {
        const abosultePath = path.join(__dirname + './../../app/public/my-uploads/' + req.file.filename);
        return res.sendFile(abosultePath);
    }
}