const helper = require('./../Helper/format');

exports.tambah = (req, res) => {
    const { nilai } = req.body;
    const hasil = nilai.reduce((acc, curr) => acc + curr, 0);
    const data = {
        operan: "tambah",
        nilai: nilai,
        hasil: hasil
    };
    console.log('hasil pertambahan: ' + hasil);
    return res.json(helper.formats("success", data));
}
exports.bagi = (req, res) => {
    let hasil;
    const { nilai1, nilai2 } = req.body;
    if (nilai2 === 0) {
        return res.json(helper.formats("failed","can not divided by zero"));
    }
    hasil = nilai1 / nilai2;
    const data = {
        operan: "bagi",
        "nilai-1": nilai1,
        "nilai-2": nilai2,
        hasil: hasil
    };
    console.log('hasil pembagian: ' + hasil);
    return res.json(helper.formats("success", data));
}
exports.kali = (req, res) => {
    const { nilai } = req.body;
    const hasil = nilai.reduce((acc, curr) => acc * curr, 1);
    const data = {
        operan: "kali",
        nilai: nilai,
        hasil: hasil
    };
    console.log('hasil perkalian: ' + hasil);
    return res.json(helper.formats("success", data));
}
exports.kurang = (req, res) => {
    const { nilai } = req.body;
    kurang = (total, num) => total - num;
    const hasil = nilai.reduce(kurang);
    const data = {
        operan: "kurang",
        nilai: nilai,
        hasil: hasil
    };
    console.log('hasil pengurangan: ' + hasil);
    return res.json(helper.formats("success", data));
}
