const multer = require('multer');

exports.error = (err, req, res, next) => {
    if (err) {
        res.json({ error:"file harus berupa gambar(jpg,jpeg,png)"});
    } else {
        return res.status(404).json({ error: "Request not found" });
    }
}