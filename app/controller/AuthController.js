const helper = require('./../Helper/format');

exports.userAuth = (req, res) => {
    const { username, password } = req.body;

    const data = {
        username: username,
        password: password
    };
    
    if (username.toLowerCase() === "username" && password.toLowerCase() === "secret") {
        return res.json(helper.formats("success",data));
    } else {
        return res.json(helper.formats("unauthorized","username or password wrong"));
    }
}